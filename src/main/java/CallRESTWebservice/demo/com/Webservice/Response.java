package CallRESTWebservice.demo.com.Webservice;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {
	@JsonProperty("code")
	private String code;
	@JsonProperty("meta")
	private  Meta meta= new Meta();
	@JsonProperty("data")
	private List<User> ls= new ArrayList<User>();
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public List<User> getLs() {
		return ls;
	}
	public void setLs(List<User> ls) {
		this.ls = ls;
	}
	@Override
	public String toString() {
		return "Response [code=" + code + ", meta=" + meta + ", ls=" + ls + "]";
	}
	public Response(String code, Meta meta, List<User> ls) {
		super();
		this.code = code;
		this.meta = meta;
		this.ls = ls;
	}
	public Response() {
		super();
	}
	
}
