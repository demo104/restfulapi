package CallRESTWebservice.demo.com.Webservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class RequestMethods {
	public String jsonGetResponse(String url) throws IOException {
		String json = null;
		URL fooResourceUrl = null;
		try {
			fooResourceUrl = new URL(url);
		} catch (MalformedURLException e) {
			System.out.println("url incorrect");
		}
		String readLine = null;
		HttpURLConnection conection = null;
		try {
			conection = (HttpURLConnection) fooResourceUrl.openConnection();
		} catch (IOException e) {
			System.out.println("khong connect duoc den webservice");
		}
		try {
			conection.setRequestMethod("GET");
		} catch (ProtocolException e) {
			System.out.println("khong connect duoc den webservice");
		}
		// conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample
		// here
		int responseCode = 0;
		try {
			responseCode = conection.getResponseCode();
		} catch (IOException e) {
			System.out.println("khong connect duoc den webservice");
		}
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(conection.getInputStream()));
			} catch (IOException e) {
				System.out.println("khong connect duoc den webservice");
			}
			StringBuffer response = new StringBuffer();
			try {
				while ((readLine = in.readLine()) != null) {
					response.append(readLine);
				}
			} catch (IOException e) {
				System.out.println("Response is null");
			}
			in.close();
			// print result
			// System.out.println("JSON String Result " + response.toString());
			json = response.toString();
		} else {
			System.out.println("GET NOT WORKED");
		}

		return json;
	}

	public void inputNewUser() {

	}

	public String jsonPostResquest(String url) throws IOException {
		String json = null;
		URL fooResourceUrl = null;
		try {
			fooResourceUrl = new URL(url);
			String readLine = null;
			HttpURLConnection conection = null;
			RestTemplate restTemplate = new RestTemplate();
			// create headers
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			// add basic authentication
			headers.add("authorization", "Bearer 954fb8f1fe5d1adf2c65b6fc2cc41f18e047887cf09b2e3bbfdbaac1b39f7bf4");
			// headers.add(headerName, headerValue);
			// build the request
			HttpEntity request = new HttpEntity(headers);
			//System.out.println("Input information for new user you want to add");
			System.out.println("Name for new user you want to add");
			String name = (new Scanner(System.in)).nextLine();
			System.out.println("gender for new user you want to add");
			String gender = (new Scanner(System.in)).nextLine();
			System.out.println("email for new user you want to add");
			String email = (new Scanner(System.in)).nextLine();
//			System.out.println("Status for new user you want to add");
//			String status = sc.nextLine();

			// request body parameters
			Map<String, Object> map = new HashMap<>();
			map.put("name", name);
			map.put("gender", gender);
			map.put("email", email);
			//map.put("status", status);
			map.put("status", "Inactive");

			// build the request
			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

			// send POST request
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
			//System.out.println(HttpStatus.CREATED);
			if (response.getStatusCode().CREATED == HttpStatus.CREATED) {
				System.out.println("Request Successful");
				json = response.getBody().toString();
				System.out.println(json);
			} else {
				System.out.println("Request Failed");
				json = response.getBody().toString();
				System.out.println(json);
			}
		} catch (MalformedURLException e) {
			System.out.println("url incorrect");
			System.out.println(json);
		}

		return json;
	}
	public String jsonUpdateResquest(String url) throws IOException {
		String json = null;
		URL fooResourceUrl = null;
		try {
			fooResourceUrl = new URL(url);
			String readLine = null;
			HttpURLConnection conection = null;
			RestTemplate restTemplate = new RestTemplate();
			// create headers
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			// add basic authentication
			headers.add("authorization", "Bearer 954fb8f1fe5d1adf2c65b6fc2cc41f18e047887cf09b2e3bbfdbaac1b39f7bf4");
			HttpEntity request = new HttpEntity(headers);
			System.out.println("Input for new user you want to update");
			System.out.println("Name for new user you want to update");
			String name = (new Scanner(System.in)).nextLine();
			System.out.println("email for new user you want to update");
			String email = (new Scanner(System.in)).nextLine();
			System.out.println("Status for new user you want to update");
			String status = (new Scanner(System.in)).nextLine();
			// request body parameters
			Map<String, Object> map = new HashMap<>();
			map.put("name", name);
			map.put("email", email);
			map.put("status", status);

			// build the request
			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
			// send PUT request
			System.out.println("url"+url);
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);
			System.out.println("status"+response.getStatusCode().toString());
			if (response.getStatusCode().toString().equals("200")) {
				System.out.println("Request Successful, response is");
				json = response.getBody().toString();
			} 
			else {
				System.out.println("Request Failed");
				json = response.getBody().toString();
				System.out.println(json);
			}
		} catch (MalformedURLException e) {
			System.out.println("url incorrect");
		}

		return json;
	}
	public String jsonGetUserResponse(String url) throws IOException {
		String json = null;
		URL fooResourceUrl = null;
		try {
			fooResourceUrl = new URL(url);
		} catch (MalformedURLException e) {
			System.out.println("url incorrect");
		}
		String readLine = null;
		HttpURLConnection conection = null;
		try {
			conection = (HttpURLConnection) fooResourceUrl.openConnection();
		} catch (IOException e) {
			System.out.println("khong connect duoc den webservice");
		}
		try {
			conection.setRequestMethod("GET");
		} catch (ProtocolException e) {
			System.out.println("khong connect duoc den webservice");
		}
		// conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample
		// here
		int responseCode = 0;
		try {
			responseCode = conection.getResponseCode();
		} catch (IOException e) {
			System.out.println("khong connect duoc den webservice");
		}
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(conection.getInputStream()));
			} catch (IOException e) {
				System.out.println("khong connect duoc den webservice");
			}
			StringBuffer response = new StringBuffer();
			try {
				while ((readLine = in.readLine()) != null) {
					response.append(readLine);
				}
			} catch (IOException e) {
				System.out.println("Response rong");
			}
			in.close();
			// print result
			// System.out.println("JSON String Result " + response.toString());
			json = response.toString();
		} else {
			System.out.println("GET NOT WORKED");
		}

		return json;
	}
	public String jsonDeleteUserResponse(String url) throws IOException {
		String json = null;
		URL fooResourceUrl = null;
		try {
			fooResourceUrl = new URL(url);
		} catch (MalformedURLException e) {
			System.out.println("url incorrect");
		}
		String readLine = null;
		HttpURLConnection conection = null;
		try {
			conection = (HttpURLConnection) fooResourceUrl.openConnection();
		} catch (IOException e) {
			System.out.println("khong connect duoc den webservice");
		}
		try {
			conection.setRequestMethod("DELETE");
		} catch (ProtocolException e) {
			System.out.println("khong connect duoc den webservice");
		}
		conection.setRequestProperty("authorization", "Bearer 954fb8f1fe5d1adf2c65b6fc2cc41f18e047887cf09b2e3bbfdbaac1b39f7bf4");
		// conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample
		// here
		int responseCode = 0;
		try {
			responseCode = conection.getResponseCode();
			System.out.println(responseCode);
		} catch (IOException e) {
			System.out.println("khong connect duoc den webservice");
		}
		StringBuffer response = new StringBuffer();
		if (responseCode == HttpURLConnection.HTTP_NO_CONTENT || responseCode==HttpURLConnection.HTTP_OK) {
			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(conection.getInputStream()));
			} catch (IOException e) {
				System.out.println("khong connect duoc den webservice");
			}
			
			try {
				while ((readLine = in.readLine()) != null) {
					response.append(readLine);
				}
			} catch (IOException e) {
				System.out.println("Response null");
			}
			in.close();
			// print result
			// System.out.println("JSON String Result " + response.toString());
			json = response.toString();
			System.out.println(json);
		}
		else {
			json = response.toString();
			
			System.out.println(json);
			System.out.println("DELETE NOT WORKED");
		}

		return json;
	}

}
