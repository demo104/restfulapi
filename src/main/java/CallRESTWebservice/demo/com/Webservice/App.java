package CallRESTWebservice.demo.com.Webservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import JDBC.Account;
import JDBC.JDBCConnection;

public class App {
	public static List<User> list = new ArrayList<User>();
	public static JDBCConnection cnn = new JDBCConnection();

	public boolean checkDuplicateAccountiD(String Id) throws SQLException {
		boolean check = true;
		ResultSet rs = cnn.select("select count(*) from users where id = " + "'" + Id + "'");
		int accountCheck = 0;
		while (rs.next()) {
			accountCheck = rs.getInt(1);
			// accountCheck++;
		}

		if (accountCheck == 0) {
			check = false;
			cnn.resultSetClose();
			System.out.println("\nUser have ID is not: " + Id + "existed in DB");
		} else {
			System.out.println("\nUser have ID is: " + Id + "existed in DB");
			cnn.resultSetClose();
		}
		return check;
	}

	public void addUserIntoDB(User us) {
		String id = us.getId();
		try {
			if (checkDuplicateAccountiD(id) == false) {
				String sql = "INSERT INTO `accountmgt`.`Users` (`id`, `name`, `created_at`, `updated_at`, `status`, `gender`, `email`) "
						+ "VALUES ('" + us.getId() + "', '" + us.getName() + "', '" + us.getCreated_at() + "', '"
						+ us.getUpdated_at() + "', '" + us.getStatus() + "', '" + us.getGender() + "', '"
						+ us.getEmail() + "')";
				System.out.println(sql);
				cnn.update(sql);
			} else {
				System.out.println("not insert User have Id is" + id);
			}

		} catch (SQLException e) {
			System.out.println("error when insert into DB");
		}
	}

	public void updateUserIntoDB(User us) {

		String id = us.getId();
		ResultSet rs = cnn.select("select * from `accountmgt`.`users` where `id` = " + us.getId() + "");
		try {
			while (rs.next()) {

				User us1 = new User();
				us1.setId(rs.getString(1));
				us1.setName(rs.getString(2));
				us1.setCreated_at(rs.getString(3));
				us1.setUpdated_at(rs.getString(4));
				us1.setStatus(rs.getString(5));
				us1.setGender(rs.getString(6));
				us1.setEmail(rs.getString(7));
				if (checkDuplicateAccountiD(id) == true && us.equals(us1) != true) {
					String sql = "UPDATE `accountmgt`.`users` SET `name` = " + "'" + us.getName() + "', `status`='"
							+ us.getStatus() + "',`email`='" + us.getEmail() + "' WHERE `id` = " + us.getId() ;

					System.out.println(sql);
					cnn.update(sql);
				} else {
					System.out.println("not Update User have Id is" + id);
				}
			}
		} catch (Exception ex) {
			System.out.println("error when update into DB");
		}
	}

	public void deleteUserIntoDB(User us) {
		String id = us.getId();
		try {
			if (checkDuplicateAccountiD(id) == true) {
				String sql = "DELETE FROM `accountmgt`.`users` WHERE (`id` = '" + us.getId() + "')";

				System.out.println(sql);
				cnn.update(sql);
			} else {
				System.out.println("not insert User have Id is" + id);
			}

		} catch (SQLException e) {
			System.out.println("error when insert into DB");
		}
	}

	public List<User> getAllsersResponse(String json, List<User> ls) {
		JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
		JSONObject obj = new JSONObject(json);
		// List<User> ls = new ArrayList<User>();
		int statuscode = obj.getInt("code");
		if (statuscode == 200) {
			String page = jsonObject.getAsJsonObject("meta").getAsJsonObject("pagination").get("total").getAsString();
			JsonObject Pagination = jsonObject.getAsJsonObject("meta").getAsJsonObject("pagination");
			JSONArray arrayOfTests = obj.getJSONArray("data");
			for (int i = 0; i < arrayOfTests.length(); i++) {
				User us = new User();
				us.setStatus(((JSONObject) arrayOfTests.get(i)).get("status").toString());
				us.setId(((JSONObject) arrayOfTests.get(i)).get("id").toString());
				us.setName(((JSONObject) arrayOfTests.get(i)).get("name").toString());
				us.setEmail(((JSONObject) arrayOfTests.get(i)).get("email").toString());
				us.setGender(((JSONObject) arrayOfTests.get(i)).get("gender").toString());
				us.setCreated_at(((JSONObject) arrayOfTests.get(i)).get("created_at").toString());
				System.out.println(((JSONObject) arrayOfTests.get(i)).get("created_at").toString());
				us.setUpdated_at(((JSONObject) arrayOfTests.getJSONObject(i)).get("updated_at").toString());
				addUserIntoDB(us);
				updateUserIntoDB(us);
				ls.add(us);
			}
		} else if (statuscode == 400) {
			System.out.println("Bad Request");
		} else if (statuscode == 404) {
			System.out.println("Not found");
		} else {
			System.out.println("error");
		}
		return ls;
	}

	public User getUserResponse(String json) {

		// GetAndPost.POSTRequest(response.toString());
		JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
		JSONObject obj = new JSONObject(json);
		User us = new User();
		int statuscode = obj.getInt("code");
		JsonObject data = jsonObject.getAsJsonObject("data");
		if (statuscode == 200) {
			us.setStatus(data.get("status").getAsString().toString());
			us.setId(data.get("id").getAsString().toString());
			us.setName(data.get("name").getAsString().toString());
			us.setEmail(data.get("email").getAsString().toString());
			us.setGender(data.get("gender").getAsString().toString());
			us.setCreated_at(data.get("created_at").getAsString().toString());
			us.setUpdated_at(data.get("updated_at").getAsString().toString());
			addUserIntoDB(us);
			System.out.println(us.toString());
			System.out.println("Add user success");
		} else {
			System.out.println(data.get("message").toString());
		}

		return us;
	}

	public User getPostResponse(String json) {

		JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
		JSONObject obj = new JSONObject(json);
		User us = new User();
		int statuscode = obj.getInt("code");
		if (statuscode == 201) {
			JsonObject data = jsonObject.getAsJsonObject("data");
			us.setStatus(data.get("status").getAsString().toString());
			// us.setStatus(data.get);
			us.setId(data.get("id").getAsString().toString());
			us.setName(data.get("name").getAsString().toString());
			us.setEmail(data.get("email").getAsString().toString());
			us.setGender(data.get("gender").getAsString().toString());
			us.setCreated_at(data.get("created_at").getAsString().toString());
			us.setUpdated_at(data.get("updated_at").getAsString().toString());
			addUserIntoDB(us);
			System.out.println(us.toString());
			// System.out.println("Add user success");
		} else {
			System.out.println("Posted invalid info");
			JSONArray arrayOfTests = obj.getJSONArray("data");
			for (int i = 0; i < arrayOfTests.length(); i++) {

				System.out.println((arrayOfTests.get(i).toString()));

			}
		}

		return us;
	}

	public int getPageResponse(String json) {
		int page = 0;
		JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
		JSONObject obj = new JSONObject(json);
		// User us = new User();
		int statuscode = obj.getInt("code");
		if (statuscode == 200) {
			JsonObject data = jsonObject.getAsJsonObject("meta").getAsJsonObject("pagination");
			page = Integer.parseInt(data.get("pages").getAsString().toString());
			// System.out.println("Add user success");
		} else {
			System.out.println("Posted invalid info");
			JSONArray arrayOfTests = obj.getJSONArray("data");
			for (int i = 0; i < arrayOfTests.length(); i++) {

				System.out.println((arrayOfTests.get(i).toString()));

			}
		}
		return page;
	}

	public User updateUserResponse(String json) {

		// GetAndPost.POSTRequest(response.toString());
		JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
		JSONObject obj = new JSONObject(json);
		User us = new User();
		int statuscode = obj.getInt("code");
		System.out.println(statuscode);

		if (statuscode == 200) {
			JsonObject data = jsonObject.getAsJsonObject("data");
			us.setStatus(data.get("status").getAsString().toString());
			us.setId(data.get("id").getAsString().toString());
			us.setName(data.get("name").getAsString().toString());
			us.setEmail(data.get("email").getAsString().toString());
			us.setGender(data.get("gender").getAsString().toString());
			us.setCreated_at(data.get("created_at").getAsString().toString());
			us.setUpdated_at(data.get("updated_at").getAsString().toString());
			try {
				updateUserIntoDB(us);
				System.out.println("Update user success");
			} catch (Exception e) {
				System.out.println("Update user doesn't success");
			}
			System.out.println(us.toString());

		} else if (statuscode == 422) {
			System.out.println("Update user doesn't success");
			JSONArray arrayOfTests = obj.getJSONArray("data");
			for (int i = 0; i < arrayOfTests.length(); i++) {
				System.out.println(((JSONObject) arrayOfTests.get(i)).toString());
			}
		} else {
			System.out.println("Update user doesn't success");
			JsonObject data = jsonObject.getAsJsonObject("data");
			System.out.println(data.get("message").getAsString().toString());
		}
		return us;
	}

	public User deleteUserResponse(String json, String id) {

		// GetAndPost.POSTRequest(response.toString());
		JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
		JSONObject obj = new JSONObject(json);
		User us = new User(id);
		int statuscode = obj.getInt("code");
		System.out.println(statuscode);

		if (statuscode == 204) {
			try {
				deleteUserIntoDB(us);
				System.out.println("Delete user success");
			} catch (Exception e) {
				System.out.println("Delete user doesn't success");
			}
			System.out.println(us.toString());
		} else {
			System.out.println("Delete user doesn't success");
			JsonObject data = jsonObject.getAsJsonObject("data");
			System.out.println(data.get("message").getAsString().toString());
		}
		return us;
	}

	public void dispalayList(List<User> ls) {
		System.out.println("List Uses are:");
		for (int i = 0; i < ls.size(); i++) {
			System.out.println((ls.get(i).toString()));
		}
	}

//	public void dispalayMessageList(List<Postmessage> ls) {
//
//		System.out.println("List Uses are:");
//		for (int i = 0; i < ls.size(); i++) {
//			System.out.println((ls.get(i).toString()));
//		}
//	}
	public static int inputOption(int temp) {
		do {

			try {
				temp = (new Scanner(System.in)).nextInt();
				// System.out.println(temp);
				if (temp == 0) {
					System.out.println("Hãy nhập từ 1 đến 7 tương ứng với những lựa chọn\n");
					// Scanner scn9 = new Scanner(System.in);
					temp = inputOption(temp);
				}
			} catch (Exception e) {
				System.out.println("Hãy nhập từ 1 đến 7 tương ứng với những lựa chọn\n");
				temp = inputOption(temp);
			}
		} while ((temp < 0) || (temp > 7));
		return temp;
	}

	public static void hThiMenu() {

		System.out.println("========================================================================");
		System.out.println("Hãy nhập từ 1 đến 7 tương ứng với những lựa chọn sau:");
		System.out.println("1. Get information of all Users");
		System.out.println("2. Get information of an User");
		System.out.println("3. Add an new User");
		System.out.println("4. Update information of an User");
		System.out.println("5. Delete an User");
		System.out.println("6. Get information of all Users have status is InActive");
		System.out.println("7. Thoát ");
	}

	public static void main(String[] args) throws IOException, SQLException {
		String connectionURL = "jdbc:mysql://127.0.0.1:3306/AccountMgt";
		DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
		List<User> ls = new ArrayList<User>();
		String User = "root";
		String Password = "root";
		cnn.connect(connectionURL, User, Password);
		RequestMethods rq = new RequestMethods();
		App ap = new App();
		int temp = 0;
		String temp2 = null;
		ap.hThiMenu();
		int valueOfInputOption = inputOption(temp);
		// System.out.println(valueOfInputOption);
		while ((temp > 1) || (temp <= 7)) {
			switch (valueOfInputOption) {
			case 1:
				// in all record ra man hinh
				String json = "https://gorest.co.in/public-api/users";
				int page = ap.getPageResponse(rq.jsonGetResponse(json));
				ls = new ArrayList<User>();
				for (int i = 0; i <= page; i++) {
					String fooResourceUrl = "https://gorest.co.in/public-api/users?page=" + i + "";
					ap.getAllsersResponse(rq.jsonGetResponse(fooResourceUrl), ls);
				}
				ap.dispalayList(ls);
				ap.hThiMenu();
				valueOfInputOption = inputOption(temp);
				break;
			case 2:
				System.out.println("insert userid you want to find");
				String userId = (new Scanner(System.in)).nextLine();
				String getUser = "https://gorest.co.in/public-api/users/" + userId;
				ap.getUserResponse(rq.jsonGetUserResponse(getUser));
				ap.hThiMenu();
				valueOfInputOption = inputOption(temp);
				break;
			case 3:
				String postURL = "https://gorest.co.in/public-api/users";
				ap.getPostResponse(rq.jsonPostResquest(postURL));
				ap.hThiMenu();
				valueOfInputOption = inputOption(temp);
				break;
			case 4:
				System.out.println("insert userid you want to update\n");
				String updateUserId = (new Scanner(System.in)).nextLine();
				String putURL = "https://gorest.co.in/public-api/users/" + updateUserId;
				ap.updateUserResponse(rq.jsonUpdateResquest(putURL));
				ap.hThiMenu();
				valueOfInputOption = inputOption(temp);
				break;
			case 5:
				System.out.println("insert userid you want to delete");
				String deleteUserId = (new Scanner(System.in)).nextLine();
				String deleteURL = "https://gorest.co.in/public-api/users/" + deleteUserId;
				ap.deleteUserResponse(rq.jsonDeleteUserResponse(deleteURL), deleteUserId);
				break;
			case 6:
				System.out.println("all gorest users are:");
				String fooResourceUrl2 = "https://gorest.co.in/public-api/users?status=Inactive";
				ls = new ArrayList<User>();
				ap.getAllsersResponse(rq.jsonGetResponse(fooResourceUrl2), ls);
				break;
			case 7:
				System.out.println("Bạn có muốn thoát chương trình không?");
				do {
					try {
						temp2 = (new Scanner(System.in)).next();
						System.out.println("temp2" + temp2);

						if (temp2.equalsIgnoreCase("yes") == true) {
							cnn.dbClose();
							System.exit(0); // thoát chương trình
							break;
						}

						else if (temp2.equalsIgnoreCase("no") == true) {
							ap.hThiMenu();
							valueOfInputOption = inputOption(temp);
						}
					} catch (Exception e) {
						System.out.println("Hãy nhập yes hoặc no" + e);
					}
					// } while ((temp2!="yes") || (temp2!="no"));
				} while (temp2.equalsIgnoreCase("yes") != true || temp2.equalsIgnoreCase("no") != true);
				break;
			}
		}
	}
}
