package CallRESTWebservice.demo.com.Webservice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pagination {
	@JsonProperty("total")
	private String total;
	@JsonProperty("pages")
	private String pages;
	@JsonProperty("page")
	private String page;
	@JsonProperty("limit")
	private String limit;
	@Override
	public String toString() {
		return "Pagination [total=" + total + ", pages=" + pages + ", page=" + page + ", limit=" + limit + "]";
	}
	public Pagination(String total, String pages, String page, String limit) {
		super();
		this.total = total;
		this.pages = pages;
		this.page = page;
		this.limit = limit;
	}
	public Pagination() {
		super();
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getPages() {
		return pages;
	}
	public void setPages(String pages) {
		this.pages = pages;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	
	

}
