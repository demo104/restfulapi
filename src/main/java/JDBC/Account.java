package JDBC;

public class Account {
	private int id;
	private String accountId;
	private String fullName;
	private String isActive;
	private String address;
	public Account(int id, String accountId, String fullName, String isActive, String address) {
		super();
		this.id = id;
		this.accountId = accountId;
		this.fullName = fullName;
		this.isActive = isActive;
		this.address = address;
	}
	public Account() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Account [id=" + id + ", accountId=" + accountId + ", fullName=" + fullName + ", isActive=" + isActive
				+ ", address=" + address + "]";
	}
	
	
}
