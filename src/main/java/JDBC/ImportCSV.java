package JDBC;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

public class ImportCSV {
	public Account ac1 = new Account();
	public CSVUtils csv = new CSVUtils();

	public static List<Account> readDataFromCustomSeperator(String file) {
		List<Account> ls = new ArrayList<Account>();
		try {
			// Create an object of file reader class with CSV file as a parameter.
			FileReader filereader = new FileReader(file);

			// create csvParser object with
			// custom seperator semi-colon
			CSVParser parser = new CSVParserBuilder().withSeparator(';').build();

			// create csvReader object with parameter
			// filereader and parser
			CSVReader csvReader = new CSVReaderBuilder(filereader).withCSVParser(parser).build();
			// Read all data at once
			List<String[]> allData = csvReader.readAll();
			allData.remove(0);
			System.out.println("row" + allData.size());
			// Print Data.
			for (String[] row : allData) {
				System.out.println(row[0]);
				String[] data = row[0].split(",");
				System.out.println("so cloumn in row" + data.length);
				if (data.length > 3 || data.length < 1) {
					System.out.println("Sai format");
				} else {
					Account ac1 = new Account();
					ac1.setAccountId(data[0]);
					ac1.setFullName(data[1]);
					ac1.setAddress(data[2]);
					System.out.println(ac1.toString());
					ls.add(ac1);
					System.out.println();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ls;
	}

	public static void exportDataFromCustomSeperator(String file, List<Account> ls) throws IOException {
		// String csvFile = "/Users/mkyong/csv/developer.csv";
		FileWriter writer = new FileWriter(file);

		// for header
		CSVUtils.writeLine(writer, Arrays.asList("AccountId", "FullName", "Address"));

		for (int i = 0; i < ls.size(); i++) {
			List<String> list = new ArrayList<>();
			list.add(ls.get(i).getAccountId());
			list.add(ls.get(i).getFullName());
			list.add(ls.get(i).getAddress());

			CSVUtils.writeLine(writer, list);

			// try custom separator and quote.
			// CSVUtils.writeLine(writer, list, '|', '\"');
		}

		writer.flush();
		writer.close();

	}
}
