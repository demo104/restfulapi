package JDBC;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AccountMgt {
	public static List<Account> list = new ArrayList<Account>();
	public static JDBCConnection cnn = new JDBCConnection();
	public static Account ac = new Account();
	public static ImportCSV ip = new ImportCSV();

	public void DisplayAccountList(List<Account> list) {
		final Object[][] table = new String[list.size()][];
		for (int i = 0; i < list.size(); i++) {
			String id = String.valueOf(list.get(i).getId());
			table[i] = new String[] { id, list.get(i).getAccountId(), list.get(i).getFullName(),
					list.get(i).getAddress(), list.get(i).getIsActive() };

		}
		System.out.println("id\taccountId\tfullName\t\taddress\t\tisActive");
		for (final Object[] row : table) {
			System.out.format("%1s%10s%25s%20s%10s\n", row);
		}

	}

	public List<Account> addAccount(ResultSet rs, List<Account> list, String sql) throws SQLException {
		rs = cnn.select(sql);
		boolean hasValue = false;
		// System.out.println("before");
		while (rs.next()) {
			hasValue = true;

			try {
				Account ac1 = new Account();
				ac1.setId(rs.getInt(1));
				ac1.setAccountId(rs.getString(2));
				ac1.setFullName(rs.getString(3));
				ac1.setAddress(rs.getString(4));
				ac1.setIsActive(rs.getString(5));
				list.add(ac1);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		System.out.println(hasValue);
		if (!hasValue) {
			System.out.println("null");
		}

		return list;
	}

	public boolean checkDuplicateAccountiD(String accountId) throws SQLException {
		boolean check = true;
		ResultSet rs = cnn.select(
				// "select count(*) from accounts where accountId LIKE " + "'%" + accountId +
				// "%'" + "and isActive='1'");
				"select count(*) from accounts where accountId = " + "'" + accountId + "'");
		int accountCheck = 0;
		while (rs.next()) {
			accountCheck = rs.getInt(1);
			// accountCheck++;
		}

		if (accountCheck == 0) {
			check = false;
			cnn.resultSetClose();
			System.out.println("không có acount nào có account nào trùng khớp với accountID là: " + accountId);
		} else {
			System.out.println("\ncó:  " + accountCheck + " account trùng khớp với accountID là: " + accountId);
			cnn.resultSetClose();
		}
		return check;
	}

	public boolean checkRecordNull(String accountId, String sql) throws SQLException {
		boolean check = true;
		ResultSet rs = cnn.select(sql);
		int countChecknull = 0;
		while (rs.next()) {
			countChecknull = rs.getInt(1);
			// accountCheck++;
		}
		if (countChecknull == 0) {
			check = false;
			cnn.resultSetClose();
			// System.out.println("không có acount nào có account nào trùng khớp với
			// accountID là: " + accountId);
		} else {
			System.out.println("\ncó " + countChecknull + " trùng khớp với accountID là:  " + accountId);
			cnn.resultSetClose();
		}
		return check;
	}

	public void inputNewAccount() throws SQLException {
		Scanner sc1 = new Scanner(System.in);
		System.out.println("Nhập những thông tin để tạo mới 1 account:");
		System.out.println("Nhâp AccoutnId:");
		String Account = sc1.nextLine();
		boolean check = checkDuplicateAccountiD(Account);

		while (check != false) {
			System.out.println("\nAccountId bạn nhập bị trùng xin nhập lại:\n");
			Scanner sc2 = new Scanner(System.in);
			Account = sc2.nextLine();
			check = checkDuplicateAccountiD(Account);
		}
		System.out.println("Nhâp fullName:");
		String fullName = sc1.nextLine();
		System.out.println("Nhâp Address:");
		String Address = sc1.nextLine();
		cnn.update("INSERT INTO accounts (accountId,fullName,address,isActive) VALUES (" + "'" + Account + "','"
				+ fullName + "','" + Address + "','1');");
		if (checkRecordNull(Account, "select count(*) from accounts where accountId ='" + Account + "'") == true) {
			System.out.println("Add Thành công");
		} else {
			System.out.println("Add không Thành công");
		}

	}

	public boolean importNewAccountFromCSV(String path) {
		boolean check = true;
		int add = 0;
		try {
			List<Account> accouts = ip.readDataFromCustomSeperator(path);
			for (int i = 0; i < accouts.size(); i++) {
				Account acc = accouts.get(i);
				// System.out.println(accouts.get(i));
				// System.out.println(checkDuplicateAccountiD(acc.getAccountId()));
				if (!checkDuplicateAccountiD(acc.getAccountId())) {
					String sql = "INSERT INTO accounts (accountId,fullName,address,isActive) VALUES (" + "'"
							+ acc.getAccountId() + "','" + acc.getFullName() + "','" + acc.getAddress() + "','1');";
					System.out.println(sql);
					System.out.println(acc.getFullName());
					cnn.update(sql);
					add++;
				}

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			check = false;
		}

		if (add > 0)
			System.out.println("Import Thành công");
		else {
			check = false;
			System.out.println("Import không Thành công");
		}
		// System.out.println("Add Thành công");
		return check;
	}

	public boolean exportNewAccountFromCSV(String path, List<Account> ls) throws SQLException {
		boolean check = true;
		try {
			ip.exportDataFromCustomSeperator(path, ls);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			check = false;
		}

		if (check == true)
			System.out.println("Export Thành công");
		else {
			System.out.println("Export Thành công");
		}
		return check;
	}

	public List<Account> updateAccount(ResultSet rs, List<Account> list) throws SQLException {
		Scanner sc3 = new Scanner(System.in);
		System.out.println("Nhập accountId b muốn Update:");
		String Account = sc3.nextLine();
		while (checkDuplicateAccountiD(Account) != true) {
			System.out.println("\nKhông có AccountId này xin nhập lại:");
			Scanner sc4 = new Scanner(System.in);
			Account = sc4.nextLine();
		}
		System.out.println("Account:" + Account);
		System.out.println("Nhâp fullName:");
		String fullName = sc3.nextLine();
		System.out.println("Nhâp Address:");
		String Address = sc3.nextLine();
		cnn.update("UPDATE accounts SET fullName=" + "'" + fullName + "',address=" + "'" + Address + "' "
				+ "Where accountId=" + "'" + Account + "';");

		System.out.println("UPDATE accounts SET fullName=" + "'" + fullName + "',address=" + "'" + Address + "' "
				+ "Where accountId=" + "'" + Account + "';");
		addAccount(rs, list, "select * from accounts;");
		System.out.println("Update Thành công");
		return list;
	}

	public void deleteAccount() {
		Scanner sc5 = new Scanner(System.in);
		System.out.println("Nhập accountId b muốn Update:");
		String Account = sc5.nextLine();
		try {
			while (checkDuplicateAccountiD(Account) != true) {
				System.out.println("Không có AccountId này xin nhập lại:");
				Scanner sc6 = new Scanner(System.in);
				Account = sc6.nextLine();
			}
			cnn.update("UPDATE accounts SET isActive='0' Where accountId=" + "'" + Account + "';");
			System.out.println("Delete Thành công");
		} catch (SQLException e) {
			System.out.println("Delete Khong Thành công");
		}

	}

	public static void hThiMenu() {

		System.out.println("========================================================================");
		System.out.println("Hãy nhập từ 1 đến 4 tương ứng với những lựa chọn sau:");
		System.out.println("1. Xem thông tin của tất cả account");
		System.out.println("2. Hiện thị thông tin account theo id nhập từ bàn phím");
		System.out.println("3. Hiện thị thông tin account theo id or fullname nhập từ bàn phím");
		System.out.println("4. Tạo mới account");
		System.out.println("5. Update thông tin account ");
		System.out.println("6. Xoa thông tin account");
		System.out.println("7. Them account tu CVS file");
		System.out.println("8. Export file ");
		System.out.println("9. Thoát ");
	}

	public static int inputOption(int temp) {

		do {
			Scanner scn7 = new Scanner(System.in);
			Scanner scn9 = new Scanner(System.in);
			Scanner scn8 = new Scanner(System.in);
			try {

				// Scanner scn7 = new Scanner(System.in);
				scn7.reset().nextInt();
				temp = scn7.nextInt();
				System.out.println(temp);
//				if (temp == 0) {
//					scn9.reset();
//					System.out.println("Hãy nhập từ 1 đến 9 tương ứng với những lựa chọn\naaa");
//					// Scanner scn9 = new Scanner(System.in);
//					temp = scn9.nextInt();
//				}
			} catch (Exception e) {
				System.out.println("Hãy nhập từ 1 đến 9 tương ứng với những lựa chọn\n");
				// Scanner scn8 = new Scanner(System.in);
				temp = scn8.nextInt();
			}
			
			
			
		} while ((temp <= 0) || (temp > 9));
		return temp;
	}

	public static void main(String[] args) throws SQLException {
		String path = "C:\\Users\\Administrator\\eclipse-workspace\\Webservice\\src\\main\\java\\JDBC\\Accounts.csv";
		AccountMgt acmgt = new AccountMgt();
		String connectionURL = "jdbc:mysql://127.0.0.1:3306/AccountMgt";
		DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
		String User = "root";
		String Password = "root";
		cnn.connect(connectionURL, User, Password);
		String sql = "select * from accounts where isActive='1';";
		Scanner scn8 = new Scanner(System.in);
		int temp = 0;
		String temp2 = null;
		acmgt.hThiMenu();
		int valueOfInputOption = inputOption(temp);
		System.out.println(valueOfInputOption);

		while ((temp > 1) || (temp <= 9)) {
			switch (valueOfInputOption) {
			case 1:
				// in all record ra man hinh

				ResultSet rs = cnn.select(sql);
				list = new ArrayList<Account>();
				acmgt.addAccount(rs, list, sql);
				System.out.println("Account co trong DB la:");
				acmgt.DisplayAccountList(list);
				cnn.resultSetClose();
				acmgt.hThiMenu();
				valueOfInputOption = inputOption(temp);
				break;
			case 2:
				list = new ArrayList<Account>();
				System.out.println("nhập AccountId bạn muốn tìm");
				Scanner sc9 = new Scanner(System.in);
				String input = sc9.nextLine();
				System.out.println(acmgt.checkDuplicateAccountiD(input));
				// if (acmgt.checkDuplicateAccountiD(input) == true) {

				String sql2 = "select * from accounts where accountId like " + "'%" + input + "%'";
				// System.out.println(sql2);
				// System.out.println("input"+input);
				ResultSet rs2 = cnn.select(sql2);
				acmgt.addAccount(rs2, list, sql2);
				System.out.println("Account co AccountId=" + "'" + input + "'" + "la:");
				acmgt.DisplayAccountList(list);
				// }
				cnn.resultSetClose();
				acmgt.hThiMenu();
				valueOfInputOption = inputOption(temp);
				break;
			case 3:
				// in ra nhung record có gullname và accountId nhu da nhap
				System.out.println("nhập accountId hoac fullname bạn muốn tìm");
				Scanner scnn = new Scanner(System.in);
				String input3 = scnn.nextLine();
				String sql3 = "select * from accounts where accountId LIKE " + "'%" + input3 + "%'"
						+ " or fullName Like " + "'%" + input3 + "%';";
				String sql4 = "select count(*) from accounts where accountId LIKE " + "'%" + input3 + "%'"
						+ " or fullName Like " + "'%" + input3 + "%';";
				// System.out.println(sql3);
				if (acmgt.checkRecordNull(input3, sql4) == true) {
					list = new ArrayList<Account>();
					ResultSet rs3 = cnn.select(sql3);
					acmgt.addAccount(rs3, list, sql3);
					System.out.println("Account co AccountId=" + input3 + "la:");
					acmgt.DisplayAccountList(list);
					acmgt.hThiMenu();
					valueOfInputOption = inputOption(temp);
				} else {

					System.out.println("khong có account nao có accountId hoac fullName nhu b nhap");
					acmgt.hThiMenu();
					valueOfInputOption = inputOption(temp);
				}
				break;
			case 4:
				//
				acmgt.inputNewAccount();
				List<Account> list4 = new ArrayList<Account>();
				ResultSet rs4 = cnn.select(sql);
				acmgt.addAccount(rs4, list4, sql);
				acmgt.DisplayAccountList(list4);
				acmgt.hThiMenu();
				valueOfInputOption = inputOption(temp);

				break;
			case 5:
				list = new ArrayList<Account>();
				ResultSet rsr = cnn.select(sql);
				acmgt.updateAccount(rsr, list);
				acmgt.DisplayAccountList(list);
				acmgt.hThiMenu();
				valueOfInputOption = inputOption(temp);
				break;
			case 6:
				list = new ArrayList<Account>();
				acmgt.deleteAccount();
				acmgt.DisplayAccountList(list);
				acmgt.hThiMenu();
				valueOfInputOption = inputOption(temp);
				break;
			case 7:
				System.out.println("Add new account from csv file");
				acmgt.importNewAccountFromCSV(path);
				acmgt.hThiMenu();
				valueOfInputOption = inputOption(temp);
				break;
			case 8:
				rs = cnn.select(sql);
				list = new ArrayList<Account>();
				acmgt.addAccount(rs, list, sql);
				// System.out.println("Account co trong DB la:");
				acmgt.exportNewAccountFromCSV(path, list);
				cnn.resultSetClose();
				acmgt.hThiMenu();
				valueOfInputOption = inputOption(temp);
			case 9:
				System.out.println("Bạn có muốn thoát chương trình không?");
				do {
					try {
						temp2 = scn8.next();
						System.out.println("temp2" + temp2);

						if (temp2.equalsIgnoreCase("yes") == true) {
							cnn.dbClose();
							System.exit(0); // thoát chương trình
							break;
						}

						else if (temp2.equalsIgnoreCase("no") == true) {
							acmgt.hThiMenu();
							valueOfInputOption = inputOption(temp);
						}
					} catch (Exception e) {
						System.out.println("Hãy nhập yes hoặc no" + e);
					}
					// } while ((temp2!="yes") || (temp2!="no"));
				} while (temp2.equalsIgnoreCase("yes") != true || temp2.equalsIgnoreCase("no") != true);
				break;
			}
		}
	}

}
