package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.mysql.cj.MysqlConnection;

public class JDBCConnection {
	private Connection conn;
    private Statement stmt;
    private ResultSet rset;
    
//    public boolean registerDriver(String sDriver) {
//        try {
//            Class.forName(sDriver);
//            return true;
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
    
    public void connect(String sHost, String sUser, String sPass) {
        try {
            this.conn = DriverManager.getConnection(sHost, sUser, sPass);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public ResultSet select(String sql) {
        try {
            this.stmt = this.conn.createStatement();
            this.rset = this.stmt
                .executeQuery(sql);
            return this.rset;
        } catch (SQLException e) {
             e.printStackTrace();
             return null;
        }
    }
    public void update(String sql) {
        try {
        	 Statement stmt = conn.createStatement();
           int rset = stmt.executeUpdate(sql);
        } catch (SQLException e) {
             e.printStackTrace();
            
        }
    }
    public void dbClose() {
        try {
            this.stmt.close();
            this.rset.close();
            this.conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void resultSetClose() {
        try {
            this.stmt.close();
            this.rset.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
